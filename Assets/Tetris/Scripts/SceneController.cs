﻿using UnityEngine;

namespace Tetris.Scripts
{
    public class SceneController : MonoBehaviour
    {
        [SerializeField] private GameObject m_nextFigurePlaceholder;
        [SerializeField] private GameObject m_currentFigureSpawnPlaceholder;
        [SerializeField] private FigureGenerator m_figureCreator;
        private Vector3 nextFigureCoordinates;
        private Vector3 currentFigureSpawnCoordinates;
        private GameObject currentFigure;
        private GameObject nextFigure;

        private void Start()
        {
            nextFigureCoordinates = m_nextFigurePlaceholder.transform.position;
            currentFigureSpawnCoordinates = m_currentFigureSpawnPlaceholder.transform.position;
        
            nextFigure = m_figureCreator.CreateRandomFigure(nextFigureCoordinates);
            currentFigure = m_figureCreator.CreateRandomFigure(currentFigureSpawnCoordinates);
        }

        private void Update()
        {
            if (currentFigure.transform.position.y < 0.0f)
            {
                Destroy(currentFigure);
                currentFigure = nextFigure;
                nextFigure.transform.position = currentFigureSpawnCoordinates;
                nextFigure = m_figureCreator.CreateRandomFigure(nextFigureCoordinates);
            }

            Vector3 position = currentFigure.transform.position;
            position.y -= 0.1f;
            currentFigure.transform.position = position;
        }
    }
}