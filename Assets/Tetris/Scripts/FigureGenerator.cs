﻿using UnityEngine;

namespace Tetris.Scripts
{
    public class FigureGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject m_baseCube;

        public GameObject CreateRandomFigure(Vector3 coordinates)
        {
            return Random.Range(0, 2) == 1 ? CreateStickFigure(coordinates) : CreateZFigure(coordinates);
        }

        private GameObject CreateStickFigure(Vector3 coordinates)
        {
            GameObject stickFigure = new GameObject("Stick Figure");
            for (int i = 0; i < 4; i++)
            {
                GameObject cube = Instantiate(m_baseCube, stickFigure.transform, true);
                cube.transform.position = new Vector3(0.0f, i, 0.0f);
            }

            stickFigure.transform.position = coordinates;
            return stickFigure;
        }

        private GameObject CreateZFigure(Vector3 coordinates)
        {
            GameObject zFigure = new GameObject("Z figure");
            for (int i = 0; i < 2; i++)
            {
                GameObject cube1 = Instantiate(m_baseCube, zFigure.transform, true);
                cube1.transform.position = new Vector3(i, 0.0f, 0.0f);
                GameObject cube2 = Instantiate(m_baseCube, zFigure.transform, true);
                cube2.transform.position = new Vector3(i - 1, 1.0f, 0.0f);
            }

            zFigure.transform.position = coordinates;
            return zFigure;
        }
    }
}