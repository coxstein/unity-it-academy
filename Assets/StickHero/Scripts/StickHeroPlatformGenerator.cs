﻿using UnityEngine;
using Random = UnityEngine.Random;

public class StickHeroPlatformGenerator : MonoBehaviour
{
    public StickHeroPlatform GeneratePlatform(GameObject lastPlatform)
    {
        GameObject platform = Instantiate(lastPlatform);
        platform.name = "Platform";
        platform.transform.localScale = new Vector3(GetRandomFloat(0.1f, 1f), 1f, 1f);
        float minDistanceBetweenPlatforms = ResolveMinDistanceBetweenPlatforms(platform, lastPlatform);
        platform.transform.position += new Vector3(GetRandomFloat(minDistanceBetweenPlatforms, 2f), 0.0f, 0.0f);
        return platform.GetComponent<StickHeroPlatform>();
    }

    private float ResolveMinDistanceBetweenPlatforms(GameObject platform1, GameObject platform2)
    {
        return (platform1.transform.localScale.x + platform2.transform.localScale.x) / 2 + 0.2f;
    }

    private float GetRandomFloat(float from, float to)
    {
        return Random.Range(from, to);
    }
}