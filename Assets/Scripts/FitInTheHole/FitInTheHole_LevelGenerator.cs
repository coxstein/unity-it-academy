﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FitInTheHole_LevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_CubePrefab;
    [SerializeField] private float m_BaseSpeed = 15f;
    [SerializeField] private float m_WallDistance = 35f;
    [SerializeField] private FitInTheHole_Template[] m_TemplatePrefabs;
    [SerializeField] private Transform m_FigurePoint;
    [SerializeField] private Text m_Score;
    [SerializeField] private Text m_Speed;

    private FitInTheHole_Template[] templates; // храним экземляры шаблонов, чтобы не инстаcтировать каждый раз
    private FitInTheHole_Template figure; //текущая фигура
    private int score;
    private float speed;
    private FitInTheHole_Wall wall;

    private void Start()
    {
        templates = new FitInTheHole_Template[m_TemplatePrefabs.Length];
        for (int i = 0; i < templates.Length; i++)
        {
            templates[i] = Instantiate(m_TemplatePrefabs[i]);
            templates[i].gameObject.SetActive(false);
            templates[i].transform.position = m_FigurePoint.position;
        }

        wall = new FitInTheHole_Wall(5, 5, m_CubePrefab);
        SetupTemplate();
        wall.SetupWall(figure, m_WallDistance);
        speed = m_BaseSpeed;

        UpdateUi();
    }

    private void Update()
    {
        wall.Parent.transform.Translate(speed * Time.deltaTime * Vector3.back);
        if (wall.Parent.transform.position.z > m_WallDistance * -1f)
        {
            return;
        }

        if (figure.IsPlayerInTarget())
        {
            score++;
            speed += 5f;
            UpdateUi();
            SetupTemplate();
            wall.SetupWall(figure, m_WallDistance);
        }
        else
        {
            speed = 0;
            var collisionCube = wall.FindCollisionCube(figure.PlayerPosition.position);
            if (collisionCube != null)
            {
                collisionCube.GetComponent<Renderer>().material.color = Color.red;
            }

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void SetupTemplate()
    {
        if (figure)
        {
            figure.gameObject.SetActive(false);
        }

        var rand = Random.Range(0, templates.Length);
        figure = templates[rand];
        figure.gameObject.SetActive(true);
        figure.SetupRandomFigure();
    }

    private void UpdateUi()
    {
        m_Score.text = $"Score: {score}";
        m_Speed.text = $"Speed: {speed}";
    }
}