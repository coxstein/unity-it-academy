﻿using UnityEngine;

public class FitInTheHole_Template : MonoBehaviour
{
    [SerializeField] private Transform[] m_Cubes;
    [SerializeField] private Transform[] m_PositionVariants;
    [SerializeField] private Transform m_PlayerPosition;
    public Transform PlayerPosition => m_PlayerPosition;

    private int currentPosition;
    private FitInTheHole_FigureTweener tweener; //экземпляр класса, отвечающий за повороты кубика
    public GameObject Target { get; private set; }
    public Transform CurrentTarget { get; private set; }

    /// <summary>
    /// Получение массива со всеми блоками фигуры
    /// </summary>
    /// <returns></returns>
    public Transform[] GetFigure()
    {
        var figure = new Transform[m_Cubes.Length + 1];
        m_Cubes.CopyTo(figure, 0);
        figure[figure.Length - 1] = CurrentTarget;
        return figure;
    }

    /// <summary>
    /// Постройка случайной фигуры
    /// </summary>
    public void SetupRandomFigure()
    {
        int rand = Random.Range(0, m_PositionVariants.Length);
        for (int i = 0; i < m_PositionVariants.Length; i++)
        {
            if (i == rand)
            {
                Target = m_PositionVariants[i].gameObject;
                Target.name = "Target";
                Target.SetActive(false);
                CurrentTarget = m_PositionVariants[i].transform;
                continue;
            }

            m_PositionVariants[i].gameObject.SetActive(false);
        }

        int randomStartPosition = Random.Range(0, m_PositionVariants.Length);
        m_PlayerPosition.position = m_PositionVariants[randomStartPosition].position;
        currentPosition = randomStartPosition;
    }


    private void Update()
    {
        if (tweener)
            return;

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            MoveLeft();

        if (Input.GetKeyDown(KeyCode.RightArrow))
            MoveRight();
    }

    private void MoveLeft()
    {
        if (!IsMovementPossible(1))
            return;

        currentPosition += 1;
        tweener = m_PlayerPosition.gameObject.AddComponent<FitInTheHole_FigureTweener>();
        tweener.Tween(m_PlayerPosition.position, m_PositionVariants[currentPosition].position,
            KeyCode.LeftArrow);
    }

    private void MoveRight()
    {
        if (!IsMovementPossible(-1))
            return;

        currentPosition -= 1;
        tweener = m_PlayerPosition.gameObject.AddComponent<FitInTheHole_FigureTweener>();
        tweener.Tween(m_PlayerPosition.position, m_PositionVariants[currentPosition].position,
            KeyCode.RightArrow);
    }

    private bool IsMovementPossible(int dir)
    {
        return currentPosition + dir >= 0 && currentPosition + dir < m_PositionVariants.Length;
    }

    //наверное можно было обойтись и m_PlayerPosition.transform.position == Target.transform.position,
    // тк погрешности в 1e-5 хватило бы в этом случае
    public bool IsPlayerInTarget()
    {
        Vector3 player = m_PlayerPosition.transform.position;
        Vector3 target = Target.transform.position;

        if (Mathf.Abs(player.x - target.x) > 0.1f)
        {
            return false;
        }

        if (Mathf.Abs(player.y - player.y) > 0.1f)
        {
            return false;
        }

        return true;
    }
}