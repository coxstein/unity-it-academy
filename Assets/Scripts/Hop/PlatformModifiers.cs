
public class PlatformModifiers
{
    public float Height { get; set; } = 1f;
    public float Speed { get; set; } = 1f;
    public float Distance { get; set; } = 2f;
}