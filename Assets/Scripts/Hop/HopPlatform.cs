﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class HopPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_activeView;

    public PlatformModifiers PlatformModifiers { get; private set; }

    private static List<PlatformModifier> modifiers =
        Enum.GetValues(typeof(PlatformModifier)).Cast<PlatformModifier>().ToList();

    private enum PlatformModifier
    {
        NoModifier,
        HeightModifier,
        SpeedModifier,
        DistanceModifier,
    }

    private void Start()
    {
        PlatformModifiers = new PlatformModifiers();

        var randomModifier = modifiers[Random.Range(0, modifiers.Count)];

        switch (randomModifier)
        {
            case PlatformModifier.NoModifier:
                SetBasePlatformColor(Color.white);
                break;
            case PlatformModifier.HeightModifier:
                SetBasePlatformColor(Color.cyan);
                PlatformModifiers.Height = 1.7f;
                break;
            case PlatformModifier.SpeedModifier:
                SetBasePlatformColor(Color.yellow);
                PlatformModifiers.Speed = 1.5f;
                break;
            case PlatformModifier.DistanceModifier:
                SetBasePlatformColor(Color.red);
                PlatformModifiers.Distance = 4f;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void Activate()
    {
        m_activeView.SetActive(true);
        Invoke(nameof(Deactivate), 0.3f);
    }

    private void Deactivate()
    {
        m_activeView.SetActive(false);
    }

    private void SetBasePlatformColor(Color color)
    {
        transform.GetChild(0).GetComponent<Renderer>().material.SetColor("_Color", color);
    }
}