﻿using System.Collections.Generic;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    [SerializeField] private bool m_useRandomSeed;
    [SerializeField] private int m_seed = 123456;
    [SerializeField] private HopPlayer m_HopPlayer;
    private List<GameObject> platforms = new List<GameObject>();
    private int lastPassedPlatformId;

    private void Start()
    {
        platforms.Add(m_Platform);

        if (m_useRandomSeed)
        {
            Random.InitState(m_seed);
        }

        for (int i = 0; i < 40; i++)
        {
            GameObject obj = Instantiate(m_Platform, transform);
            Vector3 pos = Vector3.zero;

            pos.z = 2 * (i + 1);
            pos.x = Random.Range(-1, 2);
            obj.transform.position = pos;

            obj.name = $"Platform_{i + 1}";
            platforms.Add(obj);
        }
    }
    
    private int GetTargetPlatformId(Vector3 position)
    {
        position.y = 0f;

        var targetPlatformId = 0;
        for (int i = lastPassedPlatformId + 1; i < lastPassedPlatformId + 3; i++)
        {
            var platformZ = platforms[i].transform.position.z;
            if (platformZ + 0.5f < position.z)
            {
                continue;
            }

            if (platformZ - position.z > 0.5f)
            {
                continue;
            }

            targetPlatformId = i;
            break;
        }

        return targetPlatformId;
    }

    public bool IsBallOnPlatform(Vector3 position)
    {
        var targetPlatformId = GetTargetPlatformId(position);
        var platform = platforms[targetPlatformId].GetComponent<HopPlatform>();

        float minX = platform.transform.position.x - 0.5f;
        float maxX = minX + 1f;

        if (position.x > minX && position.x < maxX)
        {
            platform.Activate();
            m_HopPlayer.ApplyPlatformModifiers(platform.PlatformModifiers);
            lastPassedPlatformId = targetPlatformId;
            return true;
        }

        return false;
    }
}