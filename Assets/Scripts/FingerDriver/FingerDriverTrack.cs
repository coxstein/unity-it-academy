﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{
    private class TrackSegment
    {
        public Vector3[] Points;

        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_LineRenderer;

    [SerializeField] private bool m_Debug;

    private Vector3[] corners;

    private TrackSegment[] segments;

    private int playerLastSegment;
    private int playerScore;

    private void Start()
    {
        //Заполняем массив опорных точек
        corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled = false;
        }

        //настраиваем Line Renderer
        m_LineRenderer.positionCount = corners.Length;
        m_LineRenderer.SetPositions(corners);

        //запекаем меш сетку из LineRenderer;
        Mesh mesh = new Mesh();
        m_LineRenderer.BakeMesh(mesh, true);

        //создаем массив сегметов трассы (каждый треугольник описан 3 точками из массива вершин)
        segments = new TrackSegment[mesh.triangles.Length / 3];
        int segmentCounter = 0;

        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            segments[segmentCounter] = new TrackSegment();
            segments[segmentCounter].Points = new Vector3[3];
            segments[segmentCounter].Points[0] = mesh.vertices[mesh.triangles[i]];
            segments[segmentCounter].Points[1] = mesh.vertices[mesh.triangles[i + 1]];
            segments[segmentCounter].Points[2] = mesh.vertices[mesh.triangles[i + 2]];

            segmentCounter++;
        }

        //отдельно можно продебажить сегменты
        if (!m_Debug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one * 0.1f;
            }
        }
    }

    /// <summary>
    /// Определяем находится ли точка в треугольнике в котором она была до этого или в одном из 2 следующих
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool IsPointInTrack(Vector3 point)
    {
        for (int i = playerLastSegment; i <= playerLastSegment + 2; i++)
        {
            if (segments[i].IsPointInSegment(point))
            {
                ProcessSegment(i);
                return true;
            }
        }

        return false;
    }

    // Проверяем новый ли сегмент и обновляем счет если надо (1 очко за каждые 2 треугольника)
    private void ProcessSegment(int segmentId)
    {
        if (segmentId > playerLastSegment)
        {
            playerLastSegment = segmentId;
            int possibleScore = segmentId / 2;
            if (possibleScore > playerScore)
            {
                playerScore = possibleScore;
                print($"Score: {playerScore}");
            }
        }
    }
}